# Youngyun Namkung

Contents Management System - **nirvrtti**

---

## Run the system

1. Run index.js. Need to install node.js and express.js `node index.js`
2. Admin: **localhost:8080/admin** Need to sign-in
3. Front: **localhost:8080/main**

---

## Manage Layout

1. Common layout files **./views/_layouts**
2. Both admin and front have default pages which cannot be deleted

---

## License

CMS uses libraries developed by the following 3rd parties:

- Express-validator • MIT • [**github**](https://github.com/express-validator/express-validator)
- Body-parser • MIT • [**github**](https://github.com/expressjs/body-parser/blob/HEAD/LICENSE)